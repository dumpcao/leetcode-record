//输入一个链表，输出该链表中倒数第k个节点。为了符合大多数人的习惯，本题从1开始计数，即链表的尾节点是倒数第1个节点。
// 例如，一个链表有6个节点，从头节点开始，
//它们的值依次是1、2、3、4、5、6。这个链表的倒数第3个节点是值为4的节点。 
//
// 
//
// 示例： 
//
// 给定一个链表: 1->2->3->4->5, 和 k = 2.
//
//返回链表 4->5. 
// Related Topics 链表 双指针

package leetcode.editor.cn;

public class LianBiaoZhongDaoShuDiKgeJieDianLcof {
    public static void main(String[] args) {
        Solution solution = new LianBiaoZhongDaoShuDiKgeJieDianLcof().new Solution();
        ListNode listNode = ListNodeUtil.constructList(new int[]{1, 2, 3, 4, 5});
        ListNode kthFromEnd = solution.getKthFromEnd(listNode, 2);
        System.out.println(kthFromEnd);
    }

    //leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    class Solution {
        public ListNode getKthFromEnd(ListNode head, int k) {
            /**
             * 双指针。
             * 假设k为2，那么，最终fast先走k-1步
             * 然后开始一起走
             */
            ListNode slow = head;
            ListNode fast = head;

            while (--k > 0 && fast != null) {
                fast = fast.next;
            }

            while (fast != null && slow != null) {

                fast = fast.next;
                if (fast == null) {
                    break;
                }
                slow = slow.next;
            }

            return slow;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}