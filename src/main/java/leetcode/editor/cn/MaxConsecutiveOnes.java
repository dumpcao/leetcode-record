//给定一个二进制数组， 计算其中最大连续1的个数。 
//
// 示例 1: 
//
// 
//输入: [1,1,0,1,1,1]
//输出: 3
//解释: 开头的两位和最后的三位都是连续1，所以最大连续1的个数是 3.
// 
//
// 注意： 
//
// 
// 输入的数组只包含 0 和1。 
// 输入数组的长度是正整数，且不超过 10,000。 
// 
// Related Topics 数组

package leetcode.editor.cn;

public class MaxConsecutiveOnes {
    public static void main(String[] args) {
        Solution solution = new MaxConsecutiveOnes().new Solution();
        int maxConsecutiveOnes = solution.findMaxConsecutiveOnes(new int[]{1, 1, 0, 1, 1, 1});
        System.out.println(maxConsecutiveOnes);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public  static final int NONE = 0;
        public  static final int START_1 = 1;
        public  static final int START_0 = 3;
        public  static final int END_0 = 4;

        public int findMaxConsecutiveOnes(int[] nums) {
            if (nums.length == 0) {
                return 0;
            }
            int maxLength = 0;
            int count = 0;
            int state = NONE;

            for (int i = 0; i < nums.length; i++) {
                int num = nums[i];
                switch (state) {
                    case NONE:{
                        if (num == 0) {
                            continue;
                        } else {
                            state = START_1;
                            count++;
                        }
                        continue;
                    }
                    case START_1:{
                        if (num == 0) {
                            state = START_0;
                            if (count > maxLength) {
                                maxLength = count;
                            }
                            count = 0;
                        } else {
                            count++;
                        }
                        continue;
                    }
                    case START_0:{
                        if (num == 1) {
                            state = START_1;
                            count = 0;
                            count++;
                        }
                        continue;
                    }

                }
            }

            if (count > maxLength) {
                maxLength = count;
            }

            return maxLength;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}