//给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数大于 ⌊ n/2 ⌋ 的元素。 
//
// 你可以假设数组是非空的，并且给定的数组总是存在多数元素。 
//
// 
//
// 示例 1: 
//
// 输入: [3,2,3]
//输出: 3 
//
// 示例 2: 
//
// 输入: [2,2,1,1,1,2,2]
//输出: 2
// 
// Related Topics 位运算 数组 分治算法

package leetcode.editor.cn;
public class MajorityElement3 {
    public static void main(String[] args) {
        Solution solution = new MajorityElement3().new Solution();
        int i = solution.majorityElement(new int[]{3,2,3,2});
        System.out.println(i);
    }
    
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int majorityElement(int[] nums) {
        if (nums == null || (nums.length == 0)) {
            return 0;
        }

        for (int i = 0; i < nums.length - 1; i++) {
            int base = nums[i];
            for (int j = i+1; j <= nums.length - 1; j++) {
                if (nums[j] < base) {
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;

                    base = nums[i];
                }
            }
        }

        if (nums.length % 2 == 0) {
            return nums[(nums.length - 1) / 2 + 1];
        } else {
            return nums[nums.length / 2];
        }

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}