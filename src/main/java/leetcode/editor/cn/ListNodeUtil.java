package leetcode.editor.cn;

public class ListNodeUtil {

    public static ListNode constructList(int[] array) {
        if (array == null || array.length == 0) {
            return null;
        }

        ListNode head = null;
        ListNode tail = null;
        for (int i = 0; i < array.length; i++) {
            if (head == null) {
                head = new ListNode(array[i]);
                tail = head;
            } else {
                ListNode node = new ListNode(array[i]);
                tail.next = node;
                tail = node;
            }
        }

        return head;
    }

    public static void printList(ListNode node) {
        if (node == null) {
            return;
        }

        while (node != null) {
            System.out.println(node.val);
            node = node.next;
        }

        System.out.println("-------------------end ");
    }

    /**
     *
     * @param array
     * @param looppos 链表尾部，要链到哪个节点去
     */
    public static ListNode constructTailLoopList(int[] array,int looppos) {
        if (array == null || array.length == 0) {
            return null;
        }

        ListNode head = null;
        ListNode tail = null;
        ListNode[] linkedArray = new ListNode[array.length];

        for (int i = 0; i < array.length; i++) {
            if (head == null) {
                head = new ListNode(array[i]);
                tail = head;

                linkedArray[i] = head;
            } else {
                ListNode node = new ListNode(array[i]);
                tail.next = node;
                tail = node;

                linkedArray[i] = node;
            }
        }

        tail.next = linkedArray[looppos];
        return head;

    }
}
