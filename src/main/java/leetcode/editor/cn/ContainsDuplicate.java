//给定一个整数数组，判断是否存在重复元素。 
//
// 如果任意一值在数组中出现至少两次，函数返回 true 。如果数组中每个元素都不相同，则返回 false 。 
//
// 
//
// 示例 1: 
//
// 输入: [1,2,3,1]
//输出: true 
//
// 示例 2: 
//
// 输入: [1,2,3,4]
//输出: false 
//
// 示例 3: 
//
// 输入: [1,1,1,3,3,4,3,2,4,2]
//输出: true 
// Related Topics 数组 哈希表

package leetcode.editor.cn;

import java.util.Arrays;

public class ContainsDuplicate {
    public static void main(String[] args) {
        Solution solution = new ContainsDuplicate().new Solution();
        boolean b = solution.containsDuplicate(new int[]{2,14,18,22,22});
        System.out.println(b);
    }
    
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean containsDuplicate(int[] nums) {
        if (nums == null || nums.length == 0) {
            return false;
        }
        Arrays.sort(nums);

        int prev = nums[0];
        for (int i = 1; i <= nums.length - 1; i++) {
            if (nums[i] == prev) {
                return true;
            }
            prev = nums[i];
        }

        return false;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}