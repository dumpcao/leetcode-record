//编写代码，移除未排序链表中的重复节点。保留最开始出现的节点。 
//
// 示例1: 
//
// 
// 输入：[1, 2, 3, 3, 2, 1]
// 输出：[1, 2, 3]
// 
//
// 示例2: 
//
// 
// 输入：[1, 1, 1, 1, 2]
// 输出：[1, 2]
// 
//
// 提示： 
//
// 
// 链表长度在[0, 20000]范围内。 
// 链表元素在[0, 20000]范围内。 
// 
//
// 进阶： 
//
// 如果不得使用临时缓冲区，该怎么解决？ 
// Related Topics 链表

package leetcode.editor.cn;

import java.util.HashSet;

public class RemoveDuplicateNodeLcci {
    public static void main(String[] args) {
        Solution solution = new RemoveDuplicateNodeLcci().new Solution();
        ListNode listNode = ListNodeUtil.constructList(new int[]{1, 1, 1, 1, 2});
        ListNode node = solution.removeDuplicateNodes(listNode);
        ListNodeUtil.printList(node);
    }
    
    //leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode removeDuplicateNodes(ListNode head) {
        HashSet<Integer> set = new HashSet<>();
        ListNode p = head;
//        while (p != null) {
//            set.add(p.val);
//            p = p.next;
//        }

        p = head;
        ListNode prev = null;
        while (p != null) {
            int val = p.val;
            if (!set.contains(val)) {
                prev = p;
                p = p.next;
                set.add(val);
            } else {
                prev.next = p.next;

                p = p.next;
            }

        }
        return head;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}