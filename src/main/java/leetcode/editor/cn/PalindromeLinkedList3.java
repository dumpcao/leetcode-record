//请判断一个链表是否为回文链表。 
//
// 示例 1: 
//
// 输入: 1->2
//输出: false 
//
// 示例 2: 
//
// 输入: 1->2->2->1
//输出: true
// 
//
// 进阶： 
//你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？ 
// Related Topics 链表 双指针

package leetcode.editor.cn;

public class PalindromeLinkedList3 {
    public static void main(String[] args) {
        Solution solution = new PalindromeLinkedList3().new Solution();
//        ListNode listNode = ListNodeUtil.constructList(new int[]{1, 0,1});
        ListNode listNode = ListNodeUtil.constructList(new int[]{1, 2,2,1});
        System.out.println(solution.isPalindrome(listNode));
    }
    
    //leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean isPalindrome(ListNode head) {
        if (head == null) {
            return true;
        }

        ListNode p = head;
        ListNode prev = null;
        while (p.next != null) {
            prev = p;
            p = p.next;
        }

        if (head == p) {
            return true;
        }

        if (head.val != p.val) {
            return false;
        }
        if (prev != null) {
            prev.next = null;
        }
        return isPalindrome(head.next);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}