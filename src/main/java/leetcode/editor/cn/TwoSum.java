//Given an array of integers, return indices of the two numbers such that they a
//dd up to a specific target. 
//
// You may assume that each input would have exactly one solution, and you may n
//ot use the same element twice. 
//
// Example: 
//
// 
//Given nums = [2, 7, 11, 15], target = 9,
//
//Because nums[0] + nums[1] = 2 + 7 = 9,
//return [0, 1].
// 
// Related Topics 数组 哈希表

package leetcode.editor.cn;

import java.util.HashMap;

public class TwoSum {
    public static void main(String[] args) {
        Solution solution = new TwoSum().new Solution();
        int[] nums =new int[]{3,3};
        int[] ints = solution.twoSum(nums, 6);
        System.out.println(ints);
    }
    
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[] twoSum(int[] nums, int target) {
        if (nums.length < 2) {
            return null;
        }

        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(nums.length);

        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i],i);
        }

        for (int i = 0; i < nums.length; i++) {
            int currentNum = nums[i];

            int left = target - currentNum;

            Integer indexOfLeftOperand = map.get(left);
            if (indexOfLeftOperand != null && indexOfLeftOperand != i) {

                return new int[]{i,indexOfLeftOperand};
            }
        }
        return null;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}