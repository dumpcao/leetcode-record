//给你一个单链表的引用结点 head。链表中每个结点的值不是 0 就是 1。
// 已知此链表是一个整数数字的二进制表示形式。
//
// 请你返回该链表所表示数字的 十进制值 。 
//
// 
//
// 示例 1： 
//
// 
//
// 输入：head = [1,0,1]
//输出：5
//解释：二进制数 (101) 转化为十进制数 (5)
// 
//
// 示例 2： 
//
// 输入：head = [0]
//输出：0
// 
//
// 示例 3： 
//
// 输入：head = [1]
//输出：1
// 
//
// 示例 4： 
//
// 输入：head = [1,0,0,1,0,0,1,1,1,0,0,0,0,0,0]
//输出：18880
// 
//
// 示例 5： 
//
// 输入：head = [0,0]
//输出：0
// 
//
// 
//
// 提示： 
//
// 
// 链表不为空。 
// 链表的结点总数不超过 30。 
// 每个结点的值不是 0 就是 1。 
// 
// Related Topics 位运算 链表

package leetcode.editor.cn;
public class ConvertBinaryNumberInALinkedListToInteger {
    public static void main(String[] args) {
        Solution solution = new ConvertBinaryNumberInALinkedListToInteger().new Solution();
        ListNode node = ListNodeUtil.constructList(new int[]{1,0,0,1,0,0,1,1,1,0,0,0,0,0,0});
        int decimalValue = solution.getDecimalValue(node);
        System.out.println(decimalValue);
    }
    
    //leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    /**
     * 可以先翻转单链表
     * 比如，假设是1,1,0,1
     * 翻过来，变成1,0,1,1
     * 然后再去遍历一遍，计算sum
     *
     * 翻转：
     * 1. 保存next
     * 2. 当前的next，指向prev
     * 3. 更新prev
     * 4. 指向第一步的next
     * @param head
     * @return
     */
    public int getDecimalValue(ListNode head) {
        ListNode current = head;
        ListNode prev = null;
        ListNode tmpNext = null;

        while (current != null) {
            tmpNext = current.next;
            current.next = prev;
            prev = current;
            current = tmpNext;
        }

        // 此时，prev就是最后那个节点，也是翻转后链表的首节点

        current = prev;
        int counter = 0;
        int sum = 0;
        while (current != null) {
            if (current.val == 1) {
                int val = current.val * powerOf2n(counter);
                sum += val;
            }
            counter++;
            current = current.next;
        }

        return sum;
    }

    int powerOf2n(int n) {
        int result = 1;
        while (n-- > 0) {
            result *= 2;
        }
        return result;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}