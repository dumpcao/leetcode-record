//给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。 
//
// 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。 
//
// 你可以假设除了整数 0 之外，这个整数不会以零开头。 
//
// 示例 1: 
//
// 输入: [1,2,3]
//输出: [1,2,4]
//解释: 输入数组表示数字 123。
// 
//
// 示例 2: 
//
// 输入: [4,3,2,1]
//输出: [4,3,2,2]
//解释: 输入数组表示数字 4321。
// 
// Related Topics 数组

package leetcode.editor.cn;
public class PlusOne {
    public static void main(String[] args) {
        Solution solution = new PlusOne().new Solution();
        int[] ints = solution.plusOne(new int[]{9});
        for (int anInt : ints) {
            System.out.println(anInt);
        }
    }
    
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[] plusOne(int[] digits) {

        int lastNumberIndex = digits.length - 1;
        int lastDigit = digits[lastNumberIndex];
        if (lastDigit + 1 <= 9) {
            digits[lastNumberIndex] = lastDigit + 1;
            return digits;
        }

        if (digits.length == 1 && digits[lastNumberIndex] == 9) {
            return new int[]{1,0};
        }
        int addon = 1;
        digits[lastNumberIndex] = 0;
        for (int i = lastNumberIndex - 1; i >= 0 ; i--) {
            /**
             * 拿到当前这一位
             */
            int number = digits[i] + addon;
            if (number  <= 9) {
                digits[i] = number;
                return digits;
            } else {
                digits[i] = 0;
                addon = 1;
                if (i == 0) {
                    int[] newIntArray = new int[digits.length + 1];
                    newIntArray[0] = 1;
                    for (int j = 1; j < newIntArray.length; j++) {
                        newIntArray[j] = digits[j-1];
                    }
                    return newIntArray;
                }
            }
        }

        return digits;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}