//给你两个有序整数数组 nums1 和 nums2，请你将 nums2 合并到 nums1 中，使 nums1 成为一个有序数组。 
//
// 
//
// 说明: 
//
// 
// 初始化 nums1 和 nums2 的元素数量分别为 m 和 n 。 
// 你可以假设 nums1 有足够的空间（空间大小大于或等于 m + n）来保存 nums2 中的元素。 
// 
//
// 
//
// 示例: 
//
// 输入:
//nums1 = [1,2,3,0,0,0], m = 3
//nums2 = [2,5,6],       n = 3
//
//输出: [1,2,2,3,5,6] 
// Related Topics 数组 双指针

package leetcode.editor.cn;
public class MergeSortedArray {
    public static void main(String[] args) {
        Solution solution = new MergeSortedArray().new Solution();
        int[] ints = {1, 2, 3, 0, 0, 0};
        solution.merge(ints,3,new int[]{2,5,6},3);
        ArrayUtil.printArray(ints);
    }
    
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int newArray[] = new int[m + n];

        int p = 0;
        int q = 0;
        int t = 0;
        while (p < m && q < n) {
            int first = nums1[p];
            int second = nums2[q];

            if (first <= second) {
                newArray[t++] = first;
                p++;
            } else {
                newArray[t++] = second;
                q++;
            }
        }

        // 说明num1数组走完了,把num2剩下的，拷贝过去
        if (p == m) {
            for (int i = q; i < n; i++) {
                newArray[t++] = nums2[i];
            }
        } else if (q == n) {
            for (int i = p; i < m; i++) {
                newArray[t++] = nums1[i];
            }
        }

        for (int i = 0; i < newArray.length; i++) {
            nums1[i] = newArray[i];
        }
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}