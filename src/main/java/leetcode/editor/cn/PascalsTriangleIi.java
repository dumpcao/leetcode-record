//给定一个非负索引 k，其中 k ≤ 33，返回杨辉三角的第 k 行。 
//
// 
//
// 在杨辉三角中，每个数是它左上方和右上方的数的和。 
//
// 示例: 
//
// 输入: 3
//输出: [1,3,3,1]
// 
//
// 进阶： 
//
// 你可以优化你的算法到 O(k) 空间复杂度吗？ 
// Related Topics 数组

package leetcode.editor.cn;

import java.util.ArrayList;
import java.util.List;

public class PascalsTriangleIi {
    public static void main(String[] args) {
        Solution solution = new PascalsTriangleIi().new Solution();
        List<Integer> row = solution.getRow(3);
        System.out.println(row);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public List<Integer> getRow(int rowIndex) {
            rowIndex = rowIndex + 1;
            return getRowInner(rowIndex);

        }

        public List<Integer> getRowInner(int rowIndex) {
            if (rowIndex == 0) {
                return new ArrayList<>();
            }

            if (rowIndex == 1) {
                ArrayList<Integer> list = new ArrayList<>();
                list.add(1);
                return list;
            } else if (rowIndex == 2) {
                ArrayList<Integer> list = new ArrayList<>();
                list.add(1);
                list.add(1);
                return list;
            }


            List<Integer> prevRow = getRowInner(rowIndex - 1);

            List<Integer> row = new ArrayList<>(rowIndex);
            row.add(1);
            for (int i = 1; i < rowIndex - 1; i++) {
                int sum = prevRow.get(i - 1) + prevRow.get(i);
                row.add(sum);
            }

            row.add(1);

            return row;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}