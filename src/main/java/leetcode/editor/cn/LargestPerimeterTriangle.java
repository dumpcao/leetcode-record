//给定由一些正数（代表长度）组成的数组 A，返回由其中三个长度组成的、面积不为零的三角形的最大周长。 
//
// 如果不能形成任何面积不为零的三角形，返回 0。 
//
// 
//
// 
// 
//
// 示例 1： 
//
// 输入：[2,1,2]
//输出：5
// 
//
// 示例 2： 
//
// 输入：[1,2,1]
//输出：0
// 
//
// 示例 3： 
//
// 输入：[3,2,3,4]
//输出：10
// 
//
// 示例 4： 
//
// 输入：[3,6,2,3]
//输出：8
// 
//
// 
//
// 提示： 
//
// 
// 3 <= A.length <= 10000 
// 1 <= A[i] <= 10^6 
// 
// Related Topics 排序 数学

package leetcode.editor.cn;

public class LargestPerimeterTriangle {
    public static void main(String[] args) {
        Solution solution = new LargestPerimeterTriangle().new Solution();
        System.out.println(solution.largestPerimeter(new int[]{2,1,2}));
        System.out.println(solution.largestPerimeter(new int[]{1,2,1}));
        System.out.println(solution.largestPerimeter(new int[]{3,2,3,4}));
        System.out.println(solution.largestPerimeter(new int[]{3,6,2,3}));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int largestPerimeter(int[] A) {
            // 排序
            quitckSort(A,0, A.length - 1);

            //
            for (int i = A.length - 1,j = i - 1,k = i - 2;
                 k >= 0; i--,j--,k--) {
                int first = A[i];
                int second = A[j];
                int third = A[k];

                if (second + third > first) {
                    return first + second + third;
                }
            }

            return 0;
        }


        private void quitckSort(int[] array, int leftBound,
                                int rightBound) {
            if (leftBound >= rightBound) {
                return;
            }

            int i = leftBound;
            int j = rightBound;
            int pivot = array[leftBound];

            while (i < j) {
                int currentOfRight;
                while ((currentOfRight = array[j]) >= pivot && i < j) {
                    j--;
                }

                int currentOfLeft;
                while ((currentOfLeft = array[i]) <= pivot && i < j) {
                    i++;
                }

                if (i < j) {
                    array[i] = currentOfRight;
                    array[j] = currentOfLeft;
                }
            }

            // 交换 array[i/j] 与array[left]
            array[leftBound] = array[i];
            array[i] = pivot;

            quitckSort(array, leftBound, i - 1);
            quitckSort(array, i + 1, rightBound);
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}