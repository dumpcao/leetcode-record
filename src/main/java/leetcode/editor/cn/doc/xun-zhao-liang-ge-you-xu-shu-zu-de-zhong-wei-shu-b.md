#### 方法：递归法

为了解决这个问题，我们需要理解 “中位数的作用是什么”。在统计中，中位数被用来：

>将一个集合划分为两个长度相等的子集，其中一个子集中的元素总是大于另一个子集中的元素。

如果理解了中位数的划分作用，我们就很接近答案了。

首先，让我们在任一位置 *i* 将 ![\text{A} ](./p__text{A}_.png)  划分成两个部分：

```
          left_A             |        right_A
    A[0], A[1], ..., A[i-1]  |  A[i], A[i+1], ..., A[m-1]
```

由于 ![\text{A} ](./p__text{A}_.png)  中有 *m* 个元素， 所以我们有 *m+1* 种划分的方法（![i=0\simm ](./p__i_=_0_sim_m_.png) ）。

我们知道：

>![\text{len}(\text{left\_A})=i,\text{len}(\text{right\_A})=m-i ](./p__text{len}_text{left_A}__=_i,_text{len}_text{right_A}__=_m_-_i_.png) .
>
>注意：当 *i = 0* 时，![\text{left\_A} ](./p__text{left_A}_.png)  为空集， 而当 *i = m* 时, ![\text{right\_A} ](./p__text{right_A}_.png)  为空集。

采用同样的方式，我们在任一位置 *j* 将 ![\text{B} ](./p__text{B}_.png)  划分成两个部分：

```
          left_B             |        right_B
    B[0], B[1], ..., B[j-1]  |  B[j], B[j+1], ..., B[n-1]
```

将 ![\text{left\_A} ](./p__text{left_A}_.png)  和 ![\text{left\_B} ](./p__text{left_B}_.png)  放入一个集合，并将 ![\text{right\_A} ](./p__text{right_A}_.png)  和 ![\text{right\_B} ](./p__text{right_B}_.png)  放入另一个集合。 再把这两个新的集合分别命名为 ![\text{left\_part} ](./p__text{left_part}_.png)  和 ![\text{right\_part} ](./p__text{right_part}_.png) ：

```
          left_part          |        right_part
    A[0], A[1], ..., A[i-1]  |  A[i], A[i+1], ..., A[m-1]
    B[0], B[1], ..., B[j-1]  |  B[j], B[j+1], ..., B[n-1]
```

如果我们可以确认：

>1. ![\text{len}(\text{left\_part})=\text{len}(\text{right\_part}) ](./p__text{len}_text{left_part}__=_text{len}_text{right_part}__.png) 
>2. ![\max(\text{left\_part})\leq\min(\text{right\_part}) ](./p__max_text{left_part}__leq_min_text{right_part}__.png) 

那么，我们已经将 ![\{\text{A},\text{B}\} ](./p__{text{A},_text{B}}_.png)  中的所有元素划分为相同长度的两个部分，且其中一部分中的元素总是大于另一部分中的元素。那么：

![\text{median}=\frac{\text{max}(\text{left}\_\text{part})+\text{min}(\text{right}\_\text{part})}{2} ](./p___text{median}_=_frac{text{max}_text{left}_text{part}__+_text{min}_text{right}_text{part}_}{2}__.png) 

要确保这两个条件，我们只需要保证：

>1. *i + j = m - i + n - j*（或：*m - i + n - j + 1*）
>如果 ![n\geqm ](./p__n_geq_m_.png) ，只需要使 ![\i=0\simm,\j=\frac{m+n+1}{2}-i\\ ](./p___i_=_0_sim_m,__j_=_frac{m_+_n_+_1}{2}_-_i__.png)  
>  
>  
>2.  ![\text{B}\[j-1\]\leq\text{A}\[i\] ](./p__text{B}_j-1__leq_text{A}_i__.png)  以及 ![\text{A}\[i-1\]\leq\text{B}\[j\] ](./p__text{A}_i-1__leq_text{B}_j__.png) 

ps.1 为了简化分析，我假设 ![\text{A}\[i-1\],\text{B}\[j-1\],\text{A}\[i\],\text{B}\[j\] ](./p__text{A}_i-1_,_text{B}_j-1_,_text{A}_i_,_text{B}_j__.png)  总是存在，哪怕出现 *i=0*，*i=m*，*j=0*，或是 *j=n* 这样的临界条件。
我将在最后讨论如何处理这些临界值。

ps.2 为什么 ![n\geqm ](./p__n_geq_m_.png) ？由于![0\leqi\leqm ](./p__0_leq_i_leq_m_.png)  且 ![j=\frac{m+n+1}{2}-i ](./p__j_=_frac{m_+_n_+_1}{2}_-_i_.png) ，我必须确保 *j* 不是负数。如果 *n < m*，那么 *j* 将可能是负数，而这会造成错误的答案。

所以，我们需要做的是：

>在 *[0，m]* 中搜索并找到目标对象 *i*，以使：
>
>![\qquad\text{B}\[j-1\]\leq\text{A}\[i\] ](./p__qquad_text{B}_j-1__leq_text{A}_i__.png)  且 ![\text{A}\[i-1\]\leq\text{B}\[j\], ](./p__text{A}_i-1__leq_text{B}_j_,_.png)  其中 ![j=\frac{m+n+1}{2}-i ](./p__j_=_frac{m_+_n_+_1}{2}_-_i_.png) 

接着，我们可以按照以下步骤来进行二叉树搜索：

1. 设 ![\text{imin}=0 ](./p__text{imin}_=_0_.png) ，![\text{imax}=m ](./p__text{imax}_=_m_.png) , 然后开始在 ![\[\text{imin},\text{imax}\] ](./p___text{imin},_text{imax}__.png)  中进行搜索。
2. 令 ![i=\frac{\text{imin}+\text{imax}}{2} ](./p__i_=_frac{text{imin}_+_text{imax}}{2}_.png) ， ![j=\frac{m+n+1}{2}-i ](./p__j_=_frac{m_+_n_+_1}{2}_-_i_.png) 
3. 现在我们有 ![\text{len}(\text{left}\_\text{part})=\text{len}(\text{right}\_\text{part}) ](./p__text{len}_text{left}_text{part}_=text{len}_text{right}_text{part}__.png) 。 而且我们只会遇到三种情况：

    - ![\text{B}\[j-1\]\leq\text{A}\[i\] ](./p__text{B}_j-1__leq_text{A}_i__.png)  且 ![\text{A}\[i-1\]\leq\text{B}\[j\] ](./p__text{A}_i-1__leq_text{B}_j__.png) ：  
      这意味着我们找到了目标对象 *i*，所以可以停止搜索。  

    - ![\text{B}\[j-1\]>\text{A}\[i\] ](./p__text{B}_j-1____text{A}_i__.png) ：  
      这意味着 ![\text{A}\[i\] ](./p__text{A}_i__.png)  太小，我们必须调整 *i* 以使 ![\text{B}\[j-1\]\leq\text{A}\[i\] ](./p__text{B}_j-1__leq_text{A}_i__.png) 。  
      我们可以增大 *i* 吗？  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;是的，因为当 *i* 被增大的时候，*j* 就会被减小。  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因此 ![\text{B}\[j-1\] ](./p__text{B}_j-1__.png)  会减小，而 ![\text{A}\[i\] ](./p__text{A}_i__.png)  会增大，那么 ![\text{B}\[j-1\]\leq\text{A}\[i\] ](./p__text{B}_j-1__leq_text{A}_i__.png)  就可能被满足。  
      我们可以减小 *i* 吗？  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;不行，因为当 *i* 被减小的时候，*j* 就会被增大。  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因此 ![\text{B}\[j-1\] ](./p__text{B}_j-1__.png)  会增大，而 ![\text{A}\[i\] ](./p__text{A}_i__.png)  会减小，那么 ![\text{B}\[j-1\]\leq\text{A}\[i\] ](./p__text{B}_j-1__leq_text{A}_i__.png)  就可能不满足。  
      所以我们必须增大 *i*。也就是说，我们必须将搜索范围调整为 ![\[i+1,\text{imax}\] ](./p___i+1,_text{imax}__.png) 。
      因此，设 ![\text{imin}=i+1 ](./p__text{imin}_=_i+1_.png) ，并转到步骤 2。

    - ![\text{A}\[i-1\]>\text{B}\[j\] ](./p__text{A}_i-1____text{B}_j__.png) ：
      这意味着 ![\text{A}\[i-1\] ](./p__text{A}_i-1__.png)  太大，我们必须减小 *i* 以使 ![\text{A}\[i-1\]\leq\text{B}\[j\] ](./p__text{A}_i-1_leq_text{B}_j__.png) 。
      也就是说，我们必须将搜索范围调整为 ![\[\text{imin},i-1\] ](./p___text{imin},_i-1__.png) 。  
      因此，设 ![\text{imax}=i-1 ](./p__text{imax}_=_i-1_.png) ，并转到步骤 2。


当找到目标对象 *i* 时，中位数为：

>![\max(\text{A}\[i-1\],\text{B}\[j-1\]), ](./p__max_text{A}_i-1_,_text{B}_j-1__,_.png)  当 *m + n* 为奇数时

>![\frac{\max(\text{A}\[i-1\],\text{B}\[j-1\])+\min(\text{A}\[i\],\text{B}\[j\])}{2}, ](./p__frac{max_text{A}_i-1_,_text{B}_j-1___+_min_text{A}_i_,_text{B}_j__}{2},_.png)  当 *m + n* 为偶数时

现在，让我们来考虑这些临界值 *i=0,i=m,j=0,j=n*，此时 ![\text{A}\[i-1\],\text{B}\[j-1\],\text{A}\[i\],\text{B}\[j\] ](./p__text{A}_i-1_,text{B}_j-1_,text{A}_i_,text{B}_j__.png)  可能不存在。
其实这种情况比你想象的要容易得多。

我们需要做的是确保 ![\text{max}(\text{left}\_\text{part})\leq\text{min}(\text{right}\_\text{part}) ](./p__text{max}_text{left}_text{part}__leq_text{min}_text{right}_text{part}__.png) 。 因此，如果 *i* 和 *j* 不是临界值（这意味着 ![\text{A}\[i-1\],\text{B}\[j-1\],\text{A}\[i\],\text{B}\[j\] ](./p__text{A}_i-1_,_text{B}_j-1_,text{A}_i_,text{B}_j__.png)  全部存在）, 那么我们必须同时检查 ![\text{B}\[j-1\]\leq\text{A}\[i\] ](./p__text{B}_j-1__leq_text{A}_i__.png)  以及 ![\text{A}\[i-1\]\leq\text{B}\[j\] ](./p__text{A}_i-1__leq_text{B}_j__.png)  是否成立。
但是如果 ![\text{A}\[i-1\],\text{B}\[j-1\],\text{A}\[i\],\text{B}\[j\] ](./p__text{A}_i-1_,text{B}_j-1_,text{A}_i_,text{B}_j__.png)  中部分不存在，那么我们只需要检查这两个条件中的一个（或不需要检查）。
举个例子，如果 *i = 0*，那么 ![\text{A}\[i-1\] ](./p__text{A}_i-1__.png)  不存在，我们就不需要检查 ![\text{A}\[i-1\]\leq\text{B}\[j\] ](./p__text{A}_i-1__leq_text{B}_j__.png)  是否成立。
所以，我们需要做的是：

>在 *[0，m]* 中搜索并找到目标对象 *i*，以使：
>
>*(j = 0* or *i = m* or ![\text{B}\[j-1\]\leq\text{A}\[i\]) ](./p__text{B}_j-1__leq_text{A}_i___.png)  或是
>*(i = 0* or *j = n* or ![\text{A}\[i-1\]\leq\text{B}\[j\]), ](./p__text{A}_i-1__leq_text{B}_j__,_.png)  其中 ![j=\frac{m+n+1}{2}-i ](./p__j_=_frac{m_+_n_+_1}{2}_-_i_.png) 

在循环搜索中，我们只会遇到三种情况：

>1. *(j = 0* or *i = m* or ![\text{B}\[j-1\]\leq\text{A}\[i\]) ](./p__text{B}_j-1__leq_text{A}_i___.png)  或是 *(i = 0* or *j = n* or ![\text{A}\[i-1\]\leq\text{B}\[j\]) ](./p__text{A}_i-1__leq_text{B}_j___.png) ，这意味着 *i* 是完美的，我们可以停止搜索。
>2. *j > 0* and *i < m* and ![\text{B}\[j-1\]>\text{A}\[i\] ](./p__text{B}_j_-_1____text{A}_i__.png)  这意味着 *i* 太小，我们必须增大它。
>3. *i > 0* and *j < n* and ![\text{A}\[i-1\]>\text{B}\[j\] ](./p__text{A}_i_-_1____text{B}_j__.png)  这意味着 *i* 太大，我们必须减小它。

感谢 @Quentin.chen 指出：![i<m\impliesj>0 ](./p__i___m_implies_j___0_.png)  以及 ![i>0\impliesj<n ](./p__i___0_implies_j___n_.png)  始终成立，这是因为：

>![m\leqn,\i<m\impliesj=\frac{m+n+1}{2}-i>\frac{m+n+1}{2}-m\geq\frac{2m+1}{2}-m\geq0 ](./p__m_leq_n,__i___m_implies_j_=_frac{m+n+1}{2}_-_i___frac{m+n+1}{2}_-_m_geq_frac{2m+1}{2}_-_m_geq_0_.png) 
>
>![m\leqn,\i>0\impliesj=\frac{m+n+1}{2}-i<\frac{m+n+1}{2}\leq\frac{2n+1}{2}\leqn ](./p__m_leq_n,__i___0_implies_j_=_frac{m+n+1}{2}_-_i___frac{m+n+1}{2}_leq_frac{2n+1}{2}_leq_n_.png) 


所以，在情况 2 和 3中，我们不需要检查 *j > 0* 或是 *j < n* 是否成立。
```java [LgcXtDv6-Java]
class Solution {
    public double findMedianSortedArrays(int[] A, int[] B) {
        int m = A.length;
        int n = B.length;
        if (m > n) { // to ensure m<=n
            int[] temp = A; A = B; B = temp;
            int tmp = m; m = n; n = tmp;
        }
        int iMin = 0, iMax = m, halfLen = (m + n + 1) / 2;
        while (iMin <= iMax) {
            int i = (iMin + iMax) / 2;
            int j = halfLen - i;
            if (i < iMax && B[j-1] > A[i]){
                iMin = i + 1; // i is too small
            }
            else if (i > iMin && A[i-1] > B[j]) {
                iMax = i - 1; // i is too big
            }
            else { // i is perfect
                int maxLeft = 0;
                if (i == 0) { maxLeft = B[j-1]; }
                else if (j == 0) { maxLeft = A[i-1]; }
                else { maxLeft = Math.max(A[i-1], B[j-1]); }
                if ( (m + n) % 2 == 1 ) { return maxLeft; }

                int minRight = 0;
                if (i == m) { minRight = B[j]; }
                else if (j == n) { minRight = A[i]; }
                else { minRight = Math.min(B[j], A[i]); }

                return (maxLeft + minRight) / 2.0;
            }
        }
        return 0.0;
    }
}
```
```python [LgcXtDv6-Python]
def median(A, B):
    m, n = len(A), len(B)
    if m > n:
        A, B, m, n = B, A, n, m
    if n == 0:
        raise ValueError

    imin, imax, half_len = 0, m, (m + n + 1) / 2
    while imin <= imax:
        i = (imin + imax) / 2
        j = half_len - i
        if i < m and B[j-1] > A[i]:
            # i is too small, must increase it
            imin = i + 1
        elif i > 0 and A[i-1] > B[j]:
            # i is too big, must decrease it
            imax = i - 1
        else:
            # i is perfect

            if i == 0: max_of_left = B[j-1]
            elif j == 0: max_of_left = A[i-1]
            else: max_of_left = max(A[i-1], B[j-1])

            if (m + n) % 2 == 1:
                return max_of_left

            if i == m: min_of_right = B[j]
            elif j == n: min_of_right = A[i]
            else: min_of_right = min(A[i], B[j])

            return (max_of_left + min_of_right) / 2.0
```


**复杂度分析**

* 时间复杂度：![O\big(\log\big(\text{min}(m,n)\big)\big) ](./p__Obig_logbig_text{min}_m,n_big_big__.png) ，  
首先，查找的区间是 *[0, m]*。
而该区间的长度在每次循环之后都会减少为原来的一半。
所以，我们只需要执行 ![\log(m) ](./p__log_m__.png)  次循环。由于我们在每次循环中进行常量次数的操作，所以时间复杂度为 ![O\big(\log(m)\big) ](./p__Obig_log_m_big__.png) 。
由于 ![m\leqn ](./p__m_leq_n_.png) ，所以时间复杂度是 ![O\big(\log\big(\text{min}(m,n)\big)\big) ](./p__Obig_logbig_text{min}_m,n_big_big__.png) 。

* 空间复杂度：*O(1)*，
我们只需要恒定的内存来存储 *9* 个局部变量， 所以空间复杂度为 *O(1)*。